import pydtp 
import os 
import yaml 
try:
    from yaml import CLoader as yamlLoader
except ImportError:
    from yaml import Loader as yamlLoader

import base64, rsa

class Client ( pydtp.Client ):
    """
    Client application for the LbAws service
    """
    def __init__ ( self, cfgfile = None ):
        self._cfgfile = cfgfile or "%s/.lbawsrc" % os.environ['HOME']
        self._cfg = {}

        
        try: 
            with open ( self._cfgfile ) as f: 
                self._cfg = yaml.load ( f.read(), Loader = yamlLoader  ) 
                self._cfg = self._cfg or {} 
        except FileNotFoundError:
            self._cfg = {}
            print ("Creating configuration file %s" % self._cfgfile ) 
            self._save_cfg() 


        pydtp.Client.__init__ ( self, ('localhost',0) ) 


    def _save_cfg ( self ): 
        with open ( self._cfgfile, 'w') as f:
            f.write ( yaml.dump ( self._cfg )  ) 

    @property 
    def orchestrator (self):
        "Internal. Return the address to the LbAws orchestrator" 
        return self._cfg['orchestrator'] if 'orchestrator' in self._cfg.keys() else None

    @orchestrator.setter 
    def orchestrator ( self, new_val ):
        "Internal. Sets the address to the LbAws orchestrator in configuration" 
        if isinstance (new_val, str) and new_val not in ['', ' ', '\t']:
            if ":" in new_val: 
                self._cfg [ 'orchestrator' ] = new_val
            else: 
                self._cfg [ 'orchestrator' ] = "%s:8080" % new_val

        elif new_val != '':
            print ("Invalid orchestrator was ignored") 

    @property 
    def username (self):
        "Internal. Return the username as defined in configuration"
        return self._cfg['user'] if 'user' in self._cfg.keys() else None

    @username.setter 
    def username ( self, new_val ):
        "Internal. Sets the username as part of the cofiguration"
        if isinstance (new_val, str) and new_val not in ['', ' ', '\t']:
            self._cfg [ 'user' ] = new_val
        elif new_val != '':
            print ("Invalid user name was ignored") 

    @property 
    def token (self):
        "Internal. Return the token"
        return self._cfg['token'] if 'token' in self._cfg.keys() else None

    @property 
    def hiddentoken (self):
        "Internal. Return a shortened token for visualization"
        return ("***%s" % self._cfg['token'][-5:]) if 'token' in self._cfg.keys() else None

    @token.setter 
    def token ( self, new_val ):
        "Internal. Sets the token"
        if isinstance (new_val, str) and len(new_val) > 10:
            self._cfg [ 'token' ] = new_val.replace(' ', '') 
        elif new_val != '':
            print ("Invalid token was ignored") 

    @property 
    def signature (self): 
        "Internal. Gets the signature"
        username = self.username.encode('utf-8') 
        privkey = rsa.PrivateKey.load_pkcs1 ( base64.b64decode ( self.token ) ) 
        return str(  base64.b64encode ( rsa.sign ( username, privkey, 'SHA-1' ) ), 'ASCII') 


    def CLI_configure ( self ):
        """
        Configure the LbAws client. 
        Ask the LHCb Computing project for username and token to access 
        these cloud resources. 
        """
        print ( "Configure the LHCb access to AWS" ) 
        self.username = input ( "LHCb username [%s]: " % self.username ) 
        self.token    = input ( "LHCb-AWS token [%s]: " % self.hiddentoken ) 
        self.orchestrator = input ( "Orchestrator address [%s]: " % self.orchestrator ) 
        self._save_cfg() 


    def CLI_ping ( self ):
        """
        Send a message to the remote orchestrator to ensure it is alive. 
        Does not require authentication.
        """
        return (self.query ( 'ping', auth = False )) 

    def CLI_list_instances ( self, cluster = None ):
        """
        List all current instances. If `cluster` is defined, it only 
        display machines belonging to that cluster. 
        """
        #return yaml.dump(self.query ( 'list_instances', auth = True, kwargs = {'cluster':cluster} )) 
        addrs = self.query ( 'addresses' ) 
        instances = self.query ( 'instanceIds' ) 
        states = self.query ( 'states' ) 
        types = self.query ( 'instanceTypes' ) 
        workers = self.query ( 'workers' ) 
        ret = ""
        for name in sorted (addrs.keys()):
            if cluster is not None and cluster not in name: continue 
            ret += ("%-10s  %-10s %-15s   %-10s  %-10s  %-20s\n" % (
                name,
                states[name],
                workers[name][:15],
                instances[name],
                types[name],
                addrs[name],
                ))

        return ret 

    def CLI_profile ( self ):
        """
        Print information  on the authenticated user.
        """
        return yaml.dump(self.query ( 'profile', auth = True ) )

    def CLI_awsKey ( self, filename = None ):
        """
        Retrieve and store in `filename` file the private key 
        associated the authenticated user. 
        By default, filename is the name of the key defined in AWS.

        For user "joe" it would be stored to `lb_joe.pem`. 
        """
        chunks = []
        privkey = rsa.PrivateKey.load_pkcs1 ( base64.b64decode ( self.token ) ) 

        for iChunk in range(10000):
            chunk = self.query ( 'awsKey', auth = True, kwargs = {'chunk': iChunk} ) 
            chunk = base64.b64decode ( chunk ) 
            chunk = rsa.decrypt ( chunk, privkey ) 
            chunks . append (chunk) 
            if len(chunks[-1]) == 0: break
            #else: print( len (chunks[-1]) )

        key = b''
        for chunk in chunks: key += chunk

        with open ( filename or "lb_%s.pem"%self.username, 'w' ) as f:
            f.write ( str(key, 'ASCII' ) ) 

        os.system ( "chmod 400 %s" % (filename or "lb_%s.pem"%self.username) )


    def CLI_start_instance ( self, name ):
        """
        Starts a given instance (a remote virtual machine) by name.
        The name of an instance is usually the name of the cluster followed by 
        a number. For example `myCluster42`.
        """
        return self.query ( 'start_instance', kwargs = {'instanceName': name} )

    def CLI_stop_instance ( self, name ):
        """
        Stops a given instance (a remote virtual machine) by name.
        The name of an instance is usually the name of the cluster followed by 
        a number. For example `myCluster42`.
        """
        return self.query ( 'stop_instance', kwargs = {'instanceName': name} )

    def CLI_start_cluster ( self, clustername, n_machines = 1000 ):
        """
        Ensure at least `n_machines` of virtual cluster `clustername` are running.

        A cluster is a set of virtualized machines with common configuration.
        Each cluster is identified by a name, for example `myCluster` and instances
        composing the cluster are named `myCluster01`, `myCluster02` ... 

        `start_cluster` starts all the machines of a given cluster, unless 
        `n_machines` is defined. If `n_machines` is defined, then `start_cluster` 
        ensures that at least `n_machines` are on. 
        """
        return self.query ( 'start_cluster', kwargs = {'cluster': clustername, 'n_machines': int(n_machines) } )

    def CLI_stop_cluster ( self, clustername ):
        """
        Stops all instances of a cluster. 
        """
        return self.query ( 'stop_cluster', kwargs = {'cluster': clustername} )

    def CLI_ssh_config ( self, first_port = 8001 ): 
        """
        Creates a configuration file to be used to connect to the cluster 
        through ssh. 

        It assumes the identity file is stored locally with its default name.

        Binds the 8080 port to a port of localhost starting from `first_port`.
        Ports are assigned incrementally starting from `first_port` which
        defaults at 8001. 

        Example.
         $ lbaws ssh_config > my_file
         $ ssh -F my_file  myCluster01 
        """
        addrs = self.query ( 'addresses' ) 
        instances = self.query ( 'instanceIds' ) 
        defaultUsers = self.query ( 'defaultUser' ) 
        ret = ''
        for iServer, (server, ip) in enumerate(addrs.items()) :
            if "." not in ip : continue 
            ret += ("""
                ## ---- %(iServer)d : %(iId)s ----
                Host %(name)s
                Hostname %(address)s
                User %(defaultUser)s
                port 22
                ForwardX11 yes
                IdentityFile %(privateKeyFile)s
                LocalForward %(port)d localhost:8888
            """ % dict(
                    iServer = iServer + 1,
                    iId = instances[server], 
                    name = server,
                    address = ip,
                    defaultUser = defaultUsers[server], 
                    privateKeyFile = "%s/lb_%s.pem" % (os.environ['PWD'], self.username),
                    port = first_port + iServer 
                ))
        
        return ret 

    def CLI_refresh_permissions (self):
        """
        Ensure the permissions a properly set an all remote machines. 
        """
        return self.query ( 'update_permissions' ) 

    def CLI_submit (self, cluster, *commands):
        """
        Submit to cluster a command for its queue
        """
        n_enqueud = int(self.query ( 'submit', kwargs = dict (
                cluster = cluster,
                cmd = " ".join(commands), 
            ) ) )
        return ("Jobs in %s queue: %d" % (cluster, n_enqueud)) 

    def query ( self, message, args = None, kwargs = None, auth = True ):
        addr, port = self.orchestrator.split(':')
        self.address = ( addr, int (port)) 

        args = args or [] 
        kwargs = kwargs or {} 
        if auth is True:
            kwargs.update ( {'username': self.username, 'signature': self.signature} ) 

        return pydtp.Client.query ( self, message, args, kwargs  ) 


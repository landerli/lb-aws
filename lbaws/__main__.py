from .Client import Client 


import argparse 

parser = argparse.ArgumentParser ()
parser.add_argument ( "cmd", nargs = 1, help = "Command" ) 
parser.add_argument ( "args", nargs = "*", help = "Command arguments" ) 

cfg = parser.parse_args() 

cmd = cfg.cmd[0] 

client = Client ()
if "CLI_%s" % cmd in dir(client):
    res = getattr (client , "CLI_%s" % cmd) ( *cfg.args ) 
    if res is None: print ("Done.") 
    else:           print (res) 
elif cmd == "help":
    import inspect 
    if len(cfg.args) == 0: 
        print ( "Available commands:" )
        print ( "\n".join( [ " - " + f[4:] for f in dir(client) if "CLI_" in f[:4] ]) )
    else:
        signature = inspect.signature(getattr(client, "CLI_%s" % cfg.args[0]))
        func_name = cfg.args[0]
        pars = [arg for arg in signature.parameters]
        par_string = []
        for par in pars:
            if signature.parameters[par].default == inspect._empty : 
                par_string.append ("<%s>" % par)
            else:
                par_string.append ("[ <%s>" % par)

        par_string += "]"*" ".join(par_string).count ("[") 
        
        header = cfg.args[0] + " " + " ".join(par_string)
        print ("\n\n  %s" % header)
        print ("  " + "-"*len (header) ) 
        print (getattr(client, "CLI_%s" % cfg.args[0]).__doc__)

else:
    print ("Unexpected command %s" % cmd) 







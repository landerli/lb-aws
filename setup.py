from distutils.core import setup 
from glob import glob 

with open ("README.md", "r") as readme:
	long_description = readme.read() 

setup ( 
	name = 'lbaws',
	version = '0.1',
	description = 'Simplified APIs to access AWS resources as LHCb users',
	long_description = long_description, 
	long_description_content_type = "text/markdown", 
	author = 'Lucio Anderlini',
	author_email = 'Lucio.Anderlini@cern.ch',
	url = 'https://gitlab.cern.ch/landerli/lb-aws',
	packages = setuptools.find_packages(),
	scripts = glob("scripts/*"), 
	classifiers = [
		"Programmin Language :: Python :: 3"
		"License :: OSI Approved :: MIT License",
		"Operating System :: OS Independent",
	],
        install_requires = [
            "boto3", 
            "botocore", 
            "numpy", 
            "PyYAML>=5.3.1", 
            "pydtp>=0.1.1", 
            "rsa>=4.0", 
			"wheel",
        ]
)


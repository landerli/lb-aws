# LHCb AWS interface 

AWS (Amazon Web Services) is a commercial cloud service provided by Amazon 
which may complement the CERN computing resources. 
In fall 2020, CERN has started a pilot project with AWS and other commercial 
cloud service providers to assess the potential benefit for CERN experiments
of mixed set of resources based on owned and cloud resources. 

The model of the AWS administration is structured in layers:
 - *CERN IT*: controls the budgetary part and assign to the 
   experiment representatives the role of "AMI users" (Amazon Machine Instance)
 - *LHCb*: as AMI user, the LHCb Computing project defines a set of clusters 
   composed by several machines with identical configuration, and assign each
   cluster to a set of LHCb users.
 - *LHCb users*: users access the allocated machines and are the ultimate 
   responsible for the activity performed on the various LHCb clusters. 

While AWS has an effective integration for handling several AMI users as a
single customer, the underlaying organization in LHCb users is responsibility 
of LHCb and the infrastructure provided by Amazon to organize accesses is rather 
weak. The package `lb-aws` provides simplified APIs to define several 
LHCb users providing them access to the LHCb instances. 

The package is currently under active development and in the future will deal 
with the special configuration of the LHCb instances. 
Currently, it simply distribute the accesses to the various clusters and allows 
to switch the machines on and off. 

## Design 
A server application runs somewhere in the Cloud on a machine with the LHCb AMI
user keys. The server application has, at least in principle, full control over
all the LHCb instances. 

The Client application, released in this package, connects to the server through 
an RSA signature using a username and a token assigned to each user by the LHCb 
Computing Project. 

The Server application acknowledge the connection of a user, retrieve from a 
database the list of clusters the connected user has access to and provide 
RSA access keys to connect to the various servers. 

The Client application encodes the information needed to connect into an ssh 
configuration file that can then be used to connect directly to the various 
machines with interactive sessions. 

## Installation 
Install the project typing
```
 $ python3 -m pip install --user git+https://gitlab.cern.ch/landerli/lb-aws.git
```

Only if it is the first time you use pip to install executables as a user, you
may need to add to the PATH environmental variable the path to the user 
executables. This is not necessary if you are running in a virtual env. 
On most platform, in bash: 
```
export PATH=$HOME/.local/bin:$PATH
```
(in case this is necessary, pip issue a WARNING with the exact path to add)

## Configuration 
Configure the interface by typing 
```
 $ lbaws configure 
```
Provide the username, the token and the orchestrator IP as provided by the 
LHCb Computing Project. 

Check that the service is active by typing
```
 $ lbaws ping
```

And ensure that your credentials give you access to the expected clusters
by typing 
```
 $ lbaws profile
```

## Interact with the running instances 
List the instances you can interact with via the command 
```
 $ lbaws list_instances
```
if you want to restrain the list to a given cluster, specify it as a further 
argument, for example
```
 $ lbaws list_instances gantrainer
```

You can switch on a single instance, say the first one of the cluster 
`gantrainer` with 
```
 $ lbaws start_instance gantrainer01
```

Similarly, you can switch off that instance with 
```
 $ lbaws stop_instance gantrainer01
```

To quickly switch on the whole cluster, use the command `start_cluster`,
for example
```
 $ lbaws start_cluster gantrainer
```
to ensure at least *n* instances of the cluster are running, specify n as 
a further argument. For example 
```
 $ lbaws start_cluster gantrainer 3
```
ensures that at least 3 machines of the cluster gantrainer are running, 
switching on only the required number of machines. 

To stop the whole cluster type:
```
 $ lbaws stop_cluster gantrainer 
```

## Addressing and accessing the machines 
To get the updated access key connected to your account, type 
```
 $ lbaws awsKey 
```
This creates a local file with your private key to amazon machines. 
Keep this key private. 

The public IP addresses of all the instances are defined when the instance
is started. Switching off and on the machine will result into a change of 
its public address. 

The address of the running instances is listed together with the instances 
via the command `list_instances`, but it is more practical to configure 
the ssh connections to the cluster through the command 
```
 $ lbaws ssh_config > my_ssh_cfg
```

This outputs in the local file `my_ssh_cfg` the current addresses and 
open ports on the remote machines, named after the cluster they belong
to. 
This allow to connect to the machines simply as:
```
 $ ssh -F my_ssh_config  gantrainer01
```

Note that `ssh_config` assumes the name of the file containing the 
private aws key is the default name assigned by lbaws. 
In case you renamed the `pem` file, you may need to specify it 
as 
```
 $ ssh -F my_ssh_config  -i my_renamed_key.pem gantrainer01
```

## Authentication problems 
Sometimes the machines end up in a state where the direct connection is 
from a given user is not authorizeds, despite lbaws says the access should 
be granted. 

The command
```
 $ lb-aws refresh_permissions
```
requests the lbaws server to update all the running instances with the list 
of authorized users. 

## Security  
In the design of lbaws an effort has been made to preserve a minimum of 
computing secuirty. Private keys are never sent through the network 
uncrypted and the user token and evaluated verified at each issued command. 

Nonetheless, the main author of lbaws is not a secuirty expert and security 
flaws may be present here and there. After all, we are all experimenting...

Hence, please, 
 - avoid handling **confidential data** through lbaws or on the aws servers
 - do not rely on the AWS storage for important results 
 - be prepared agains availability issues that may derive from sudden 
   interruption of the service. 

## Licence 
See LICENCE

## Contributing 
Contributions, including comments and remarks, are super welcome. 
Comment here on GitLab or send a message to Lucio Anderlini [at] cern.ch






